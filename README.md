**Welcome to the drug-drug recommender repo!**

This is a project where we try to first find drug-drug similarity and then
recommend pairs of drugs with high similarity but only minor interactions.

Contributors:

Alex Wade, Lea Jabbour, Marcos Torres
